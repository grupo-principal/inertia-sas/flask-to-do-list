# Flask To Do List
## Con Docker Compose
### Modo desarrollo
#### Iniciar contenedor
```
docker-compose up --build -d
```
#### Hacerle seguimiento a los logs
```
docker-compose logs -f
```
#### Ingresar al bash del container
```
docker-compose exec app bash
```
#### Finalizar
```
docker-compose down
```
#### Cloud-SDK

Ingresar al bash del container y correr
```
gcloud auth login
```
Copiar el mensaje de respuesta en el navegador, y autenticarse con la cuenta de Google

Repetir el proceso con 
```
gcloud auth application-default login
```
##### Referencias
- https://hub.docker.com/r/google/cloud-sdk
- https://cloud.google.com/docs/authentication/
- https://googleapis.dev/python/google-api-core/latest/auth.html
- https://cloud.google.com/sdk/docs/authorizing
#### Para iniciar Flask
```
flask run --host=0.0.0.0 
```

### Modo test
#### Iniciar contenedor
```
docker-compose -f docker-compose.yml -f docker-compose.test.yml up --build
```

## Sin Docker Compose
### Modo desarrollo
#### Crear Imagen
```
docker build -t flask_app .
```
#### Crear Contenedor
```
docker  run --rm -d -p 5000:5000 -v ${PWD}/application:/application --name app1 --env FLASK_ENV=development flask_app
```
